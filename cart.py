import pytest
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC




@pytest.fixture()
def wd(request):
    wd = webdriver.Chrome()
    request.addfinalizer(wd.quit)
    return wd


def test_cart(wd):
    wd.implicitly_wait(10)
    wait = WebDriverWait(wd, 10)
    wd.get("http://localhost/litecart/en/")
    wd.find_element_by_xpath("//a[@title='Green Duck']").click()
    button = wd.find_element_by_xpath("//button[@value='Add To Cart']")
    wait.until(EC.visibility_of(button))
    button.click()
    element = wd.find_element_by_xpath("//a/span[@style]")
    wait.until(EC.visibility_of(element))
    element.click()

    wd.find_element_by_xpath("//i[@title='Home']").click()
    wd.find_element_by_xpath("//a[@title='Purple Duck']").click()
    button2 = wd.find_element_by_xpath("//button[@value='Add To Cart']")
    wait.until(EC.visibility_of(button2))
    button2.click()
    element2 = wd.find_element_by_xpath("//a/span[@style]")
    wait.until(EC.visibility_of(element2))
    element2.click()

    wd.find_element_by_xpath("//i[@title='Home']").click()
    wd.find_element_by_xpath("//a[@title='Red Duck']").click()
    button3 = wd.find_element_by_xpath("//button[@value='Add To Cart']")
    wait.until(EC.visibility_of(button3))
    button3.click()
    element3 = wd.find_element_by_xpath("//a/span[@style]")
    wait.until(EC.visibility_of(element3))
    element3.click()
    images = len(wd.find_elements_by_xpath("//li[@class='item']"))
    for index in range(images):
        wait = WebDriverWait(wd, 10)
        m = wd.find_element_by_xpath("//table[@class='dataTable rounded-corners']")
        item = m.find_element_by_xpath("//td[@class='item']")
        wait.until(EC.visibility_of(wd.find_element_by_xpath("//button[@value='Remove']"))).click()
        wait.until(EC.staleness_of(item))
