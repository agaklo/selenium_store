import pytest
from selenium import webdriver
import time


@pytest.fixture()
def wd(request):
    wd = webdriver.Chrome()
    request.addfinalizer(wd.quit)
    return wd


def test_campaigns(wd):
    wd.get("http://localhost/litecart/en/")
    time.sleep(5)
    content = wd.find_elements_by_id("box-campaigns")
    for j in content:
        name = [j.find_element_by_class_name("name").get_attribute("textContent")]
        manufacturer = [j.find_element_by_class_name("manufacturer").get_attribute("textContent")]
        first_price = j.find_element_by_class_name("regular-price")
        price1_value = [first_price.get_attribute("textContent")]
        b = first_price.value_of_css_property("color")
        c = first_price.value_of_css_property("text-decoration-line")
        price1_styles = [b + ", " + c]
        end_price = j.find_element_by_class_name("campaign-price")
        price2_value = [end_price.get_attribute("textContent")]
        d = end_price.value_of_css_property("color")
        e = end_price.value_of_css_property("font-weight")
        price2_styles = [d + ", " + e]
        j.find_element_by_css_selector(".link").click()
        time.sleep(3)
        name2 = [wd.find_element_by_xpath("//h1[@class = 'title']").get_attribute("textContent")]
        manufacturer2 = [wd.find_element_by_css_selector(".manufacturer a img").get_attribute("title")]
        first_price2 = wd.find_element_by_class_name("regular-price")
        price1_value2 = [first_price2.get_attribute("textContent")]
        b2 = first_price2.value_of_css_property("color")
        c2 = first_price2.value_of_css_property("text-decoration-line")
        price1_styles2 = [b2 + ", " + c2]
        end_price2 = wd.find_element_by_class_name("campaign-price")
        price2_value2 = [end_price2.get_attribute("textContent")]
        d2 = end_price2.value_of_css_property("color")
        e2 = end_price2.value_of_css_property("font-weight")
        price2_styles2 = [d2 + ", " + e2]

        if name == name2:
            print("names are equal")
        else:
            print("names are not equal")
        if manufacturer == manufacturer2:
            print("manufacturers are equal")
        else:
            print("manufacturers are not equal")
        if price1_value == price1_value2:
            print("prices before discount are equal")
        else:
            print("prices before discount are not equal")
        if price1_styles == price1_styles:
            print("price styles before discount are equal")
        else:
            print("price styles before discount are not equal")
        if price2_value == price2_value2:
            print("prices after discount are equal")
        else:
            print("prices after discount are not equal")
        if price2_styles == price2_styles2:
            print("price styles after discount are equal")
        else:
            print("price styles after discount are not equal")


