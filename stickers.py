import pytest
from selenium import webdriver
import time


@pytest.fixture()
def wd(request):
    wd = webdriver.Chrome()
    request.addfinalizer(wd.quit)
    return wd


def test_stickers(wd):
    wd.get("http://localhost/litecart/en/")
    time.sleep(5)
    content = wd.find_elements_by_class_name("product")
    for i in content:
        l = [wd.find_elements_by_class_name("sticker")]
        if len(l) == 1:
            print("ok - just one sticker")
        else:
            print("error - more than one sticker")


