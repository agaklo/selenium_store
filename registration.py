import pytest
from selenium import webdriver
import time
from random import *


@pytest.fixture()
def wd(request):
    wd = webdriver.Chrome()
    request.addfinalizer(wd.quit)
    return wd



def test_add_new_user(wd):
    char = 'abcdefghijklmnopqrstuvwxyz'
    min_char = 3
    max_char = 5
    email = "".join(choice(char) for x in range(randint(min_char, max_char))) + "@email.com"
    password = "1234"
    wd.get("http://localhost/litecart/en/")
    time.sleep(2)
    wd.find_element_by_link_text("New customers click here").click()
    wd.find_element_by_css_selector("[name=firstname]").send_keys("Agata")
    time.sleep(1)
    wd.find_element_by_css_selector("[name=lastname]").send_keys("Klopotowska")
    time.sleep(1)
    wd.find_element_by_css_selector("[name=address1]").send_keys("Street 1")
    time.sleep(1)
    wd.find_element_by_css_selector("[name=postcode]").send_keys("01-234")
    time.sleep(1)
    wd.find_element_by_css_selector("[name=city]").send_keys("Warsaw")
    time.sleep(1)
    wd.find_element_by_css_selector(".selection").click()
    time.sleep(1)
    wd.find_element_by_css_selector("[type=search]").send_keys("Poland" + "\n")
    time.sleep(1)
    wd.find_element_by_css_selector("[name=email]").send_keys(email)
    time.sleep(1)
    wd.find_element_by_css_selector("[name=phone]").send_keys("111222333")
    time.sleep(1)
    wd.find_element_by_css_selector("[name=password]").send_keys(password)
    time.sleep(1)
    wd.find_element_by_css_selector("[name=confirmed_password]").send_keys(password)
    time.sleep(1)
    wd.find_element_by_css_selector("[name=create_account]").click()
    time.sleep(3)
    wd.find_element_by_link_text("Logout").click()
    time.sleep(1)
    wd.find_element_by_css_selector("[name=email]").send_keys(email)
    time.sleep(1)
    wd.find_element_by_css_selector("[name=password]").send_keys(password)
    time.sleep(1)
    wd.find_element_by_css_selector("[name=login]").click()
    time.sleep(1)
    wd.find_element_by_link_text("Logout").click()
